package sample;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public HBox controls;
    @FXML
    StackPane root;
    @FXML
    WebView webView;

    private final String PRIVADO_URL = "http://interactive.privadoegypt.com/Residential/360Images/C4/output/index.html";
//    private final String PRIVADO_URL = "http://google.com";

    private WebEngine engine;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        controls.setPadding(new Insets(10));

        engine = webView.getEngine();
        engine.setJavaScriptEnabled(true);

        engine.setOnError(event -> {
            System.out.println(event.getMessage());
        });
        engine.load(PRIVADO_URL);

        //to load html or js
//        engine.loadContent(String content,String contentType);

        engine.getLoadWorker().exceptionProperty().addListener(new ChangeListener<Throwable>() {
            @Override
            public void changed(ObservableValue<? extends Throwable> ov, Throwable t, Throwable t1) {
                if(t1!=null)
                    System.out.println("Received exception: "+t1.getMessage());
            }
        });

        webView.addEventFilter(ScrollEvent.ANY, event -> {
            if(event.getTouchCount()<=1)
                event.consume();
        });

    }

    @FXML
    public void goBack(MouseEvent event) {
        Platform.runLater(() -> {
            engine.executeScript("history.back()");
        });
    }

    @FXML
    public void goHome(MouseEvent event) {
        engine.load(PRIVADO_URL);
    }

    @FXML
    public void goForward(MouseEvent event) {
        Platform.runLater(() -> {
            engine.executeScript("history.forward()");
        });
    }

}
